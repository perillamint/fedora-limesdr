#!/bin/bash

set -e

pushd limesuite
rpmbuild --undefine=_disable_source_fetch -ba limesuite.spec
popd
