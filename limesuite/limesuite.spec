#
# spec file for package limesuite
#
# Copyright (c) 2018 SUSE LINUX GmbH, Nuernberg, Germany.
# Copyright (c) 2017, Martin Hauke <mardnh@gmx.de>
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#


%define sover 19_04-0
%define libname libLimeSuite%{sover}
%define soapy_modver 0.7
Name:           limesuite
Version:        19.04.0
Release:        1.1
Summary:        Collection of software supporting LMS7-based hardware
License:        Apache-2.0
Group:          Productivity/Hamradio/Other
URL:            https://myriadrf.org/projects/lime-suite/
#Git-Clone:     https://github.com/myriadrf/LimeSuite.git
Source:         https://github.com/myriadrf/LimeSuite/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.xz
BuildRequires:  cmake
BuildRequires:  gcc-c++
BuildRequires:  gnuplot
BuildRequires:  i2c-tools
BuildRequires:  pkgconfig
BuildRequires:  wxGTK3-devel
BuildRequires:  pkgconfig(SoapySDR)
BuildRequires:  pkgconfig(libusb-1.0)
BuildRequires:  pkgconfig(sqlite3)
BuildRequires:  pkgconfig(udev)

%description
Lime Suite is a collection of software supporting several hardware
platforms including the LimeSDR, drivers for the LMS7002M transceiver
RFIC, and other tools for developing with LMS7-based hardware. Lime
Suite enables many SDR applications, such as GQRX for example, to
work with supported hardware through the bundled SoapySDR support
module.

%package -n %{libname}
Summary:        Library for Lime Suite
Group:          System/Libraries
Requires:       %{name}-udev

%description -n %{libname}
Lime Suite is a collection of software supporting several hardware
platforms and other tools for developing with LMS7-based hardware.

%package udev
Summary:        Udev rules for LimeSDR
Group:          Hardware/Other

%description udev
Udev rules for Lime Suite

%package devel
Summary:        Development files for libLimeSuite
Group:          Development/Libraries/C and C++
Requires:       %{libname} = %{version}

%description devel
Libraries and header files for developing applications that want to make
use of libLimeSuite.

%package -n soapysdr%{soapy_modver}-module-lms7
Summary:        SoapySDR LMS7 support module
Group:          System/Libraries

%description -n soapysdr%{soapy_modver}-module-lms7
Soapy LMS7 - LimeSDR device support for Soapy SDR.
A Soapy module that supports LimeSDR devices within the Soapy API.

%prep
%setup -q -n LimeSuite-%{version}

# HACK: set udev permissions to 666
sed -i 's|MODE="660"|MODE="666"|g' udev-rules/64-limesuite.rules

%build
export CFLAGS='%{optflags} -Wno-return-type'
%cmake \
  -DBUILD_SHARED_LIBS=ON \
  -DCMAKE_AUTOSET_INSTALL_RPATH=FALSE \
  -DUDEV_RULES_PATH=%{_udevrulesdir} \
%ifarch x86_64
  -DENABLE_SIMD_FLAGS="SSE4.2" \
%else
  -DENABLE_SIMD_FLAGS="none" \
%endif
  -DLIME_SUITE_EXTVER=release
%make_build

%install
%make_install

%post -n %{libname} -p /sbin/ldconfig
%postun  -n %{libname} -p /sbin/ldconfig
%post udev
%udev_rules_update

%postun udev
%udev_rules_update

%files
%license COPYING
%doc Changelog.txt README.md
%{_bindir}/LimeUtil
%{_bindir}/LimeSuiteGUI
%{_bindir}/LimeQuickTest
%dir %{_datadir}/Lime
%{_datadir}/Lime/Desktop

%files udev
%{_udevrulesdir}/64-limesuite.rules

%files -n %{libname}
%{_libdir}/libLimeSuite.so.*

%files devel
%{_libdir}/libLimeSuite.so
%{_includedir}/lime
%{_libdir}/pkgconfig/LimeSuite.pc
%{_libdir}/cmake/LimeSuite/

%files -n soapysdr%{soapy_modver}-module-lms7
%dir %{_libdir}/SoapySDR
%dir %{_libdir}/SoapySDR/modules%{soapy_modver}
%{_libdir}/SoapySDR/modules%{soapy_modver}/libLMS7Support.so

%changelog
* Wed Jun 13 2018 mardnh@gmx.de
- Update to version 18.06.0
  LimeSuite library changes:
  * Fixed zero filled packet transmitted on Tx FIFO timeout
  * Fixed LMS_GetFrequency() to return correct frequency in TDD mode
  * Added HBD/HBI bypass mode for LimeSDR-USB
  * Made automatic Tx gain calibrations less frequent
  * Fixed some issues with GFIR usage
  * Some fixes for LimeSDR-QPCIe
  SoapyLMS changes:
  * Fixed writeSetting to not throw every time
  * Added 'skipCal' stream argument to disable calibration
  * Added 'CALIBRATE' setting for writeSetting()
  Other changes:
  * Added LimeSDR-USB support to LimeQuickTest
  * Added sample format and Tx sync selection to FFTviewer
  * Fix LimeSuiteConfig for debian packages
  * LimeSDR-USB v1.4 updated to r2.17 gateware
  * LimeSDR-Mini v1.1 updated to r1.25 gateware
* Thu Apr 19 2018 mardnh@gmx.de
- Update to version 18.04.1
  * Add multiple device support for LimeSDR-Mini
  * Add QuickTest Utility for LimeSDR-Mini
  * Remove calibration cache database
  * LimeSuiteGUI: Remove duplicate information from 'Device Info' panel
- Remove patch:
  * limesuite-fix-cmake-path.patch (fixed upstream)
* Mon Apr  9 2018 mpluskal@suse.com
- Cleanup with spec-cleaner
* Fri Apr  6 2018 mardnh@gmx.de
- Update to version 18.04.0
  * Fixed Tx filter calibration
  * Fixed FPGA PLL configuration when B channel is disabled
  * Fixed non-working controls in LimeSuiteGUI 'TRX Gain' tab
  * Changed CGEN VCO tune algorithm
  * LimeSDR-USB v1.4 updated to r2.16 gateware
- Add patch:
  * limesuite-fix-cmake-path.patch
- Update to version 18.03.0
  SoapyLMS changes:
  * Do not tune NCO when LO is within tuning range
  * Filter BW reported to match filtersCalibration
  * Function implementations changed to use same code as LMS API
  LimeSuiteGUI changes:
  * Added log levels
  * Added additional controls to SPI panel
  * Reduced number of programming modes in Programming panel
  LimeSuite library changes:
  * Added LimeSDR-Mini programming
  * Changed default settings for LimeSDR-Mini
  * Updated calibrations
  * Add phase alignment for dual channel configuration
  * Updated FPGA PLL configuration
  * Fix B channel only streaming
  * Fixed LMS7002M::GetPathRFE for LNAH case
  * Fixed CGEN not tuning to 484-487 MHz
  LMS API changes:
  * LMS_Open() can no longer succeed without connecting to hardware
  * LMS_Disconnect() and LMS_IsOpen() are now deprecated because of above change
  * Changed firmware/gateware programming functions
  * Changed lms_dev_info_t structure
  * LMS_SetLOFrequency() now attempts to set different frequencies for A/B channels using NCO
  * Fix Rx NCO direction flip (downconvert flag used to set Rx NCO to upconvert)
  * Disabling LPF is now same as setting it to maximum bandwidth
  * Extented gain range for LMS_SetGaindB()
  Other changes:
  * Reduce SIMD flags when packaging for x86
  * LimeSDR-USB v1.4 updated to r2.15 gateware
  * LimeSDR-Mini v1.1 updated to r1.24 gateware
- Add limesuite-fix-cmake-path.patch
* Tue Dec 19 2017 mardnh@gmx.de
- Update to version 17.12.0
  SoapyLMS changes:
  * Force MIMO alignment
  * Add TBB frontend gain configuration
  * Fix/adjust gain ranges
  LMS API changes:
  * Increased USB timeouts
  * Updated FTDI library (LimeSDR-Mini on Windows)
  * RF switch control for LimeSDR-Mini
  * Updated setup/destruction of streams
  * various bug fixes
  LimeSuiteGUI changes:
  * Update LimeSDR-Mini panel
* Sun Nov  5 2017 jengelh@inai.de
- Update descriptions.
* Sat Oct 21 2017 mardnh@gmx.de
- Use SSE-SIMD-FLAGS only on x86_64 machines
* Thu Oct 19 2017 mardnh@gmx.de
- Update to version 17.10.0
  SoapyLMS changes:
  * Initial support for LimeSDR-Mini
  * Disable calibration cache by default
  LMS API changes:
  * Initial support for LimeSDR-Mini
  LimeSuiteGUI changes:
  * Added scrollbars
  * Fix multiple crashes that occured when board is not connected
  * Bring window to front when trying to open already opened window
  * Some cosmetic GUI changes
  * Added default config button to GUI
  Other changes:
  * LimeSDR v1.4 updated to r2.11 gateware
- Build with -DENABLE_SIMD_FLAGS="SSE4.2" to avoid the new default
  optimization settings "-mmarch=native" otherwise it the binaries
  are expected to crash on machines different from the build hosts.
* Sun Oct  1 2017 mardnh@gmx.de
- Update to version 17.09.1
  SoapyLMS changes:
  - SoapyLMS7 caches stream enable and disables on close
  - Fixed late packet reporting in SoapyLMS7 stream status
  LMS API changes:
  - Added simple GPIO example utilizing functions from LimeSuite.h
  - Fixed WriteStream() end of burst flushing in fifo.h
  - Fixed abs() overload compiler issue in lms7_device.cpp
  - Removed private include in mcu_program/host_src/main.cpp
  - Fixed error checking bug causing compilation error on MacOS in
    LMS_GetGaindB() and LMS_GetNormalizedGain()
  Other changes:
  - LimeSDR v1.4 updated to r2.10 gateware
- Update to version 17.09.0
  LimeSuite library changes:
  - Added transfer size adjustment based on sample rate
  - Improved MCU calibration procedures
  - Initial support for LimeSDR-QPCIe
  - Changed format to 16 bit for data transfer when float (non-native) format
    is selected in API
  LMS API changes:
  - Added external reference clock(LMS_CLOCK_EXTREF) configuration to
    LMS_SetClockFreq()
  - Change LMS_SetGaindB() and LMS_SetNormalizedGain() to select optimal
    TBB gain for TX
  - Fixed LMS_GetStreamStatus() not returning some status values
  LimeSuiteGUI changes:
  - Unified read/write params in board controls panel
  - Fixed some GUI fields not updating correctly/reporting wrong values
  Other changes:
  - LimeSDR v1.4 updated to r2.9 gateware
  - Added LMS API doxygen generation to CMake
  - Added CMake option to enable compiler SIMD flags
* Wed Jun 21 2017 mardnh@gmx.de
- Update to version 17.06.0
  API additions:
  - LMS_GPIODirRead
  - LMS_GPIODirWrite
  - LMS_CalibrateInternalADC
  - LMS_CalibrateAnalogRSSIDC
  - LMS_CalibrateRP_BIAS
  - LMS_CalibrateTxGain
  - LMS_SetClockFreqWithSpurCancelation
  General changes:
  - SoapyLMS7 - added getMasterClockRates()
  - SoapyLMS7 - added getSampleRateRange()
  - LimeSDR v1.4 updated to r2.8 gateware
  - GPIO support in SoapyLM7 module
  - Library level logging support
  - Speed up x640,x641 spi
  - FFTviewer: add checkbox for RX->TX loopback control
  - Merged major MCU based calibrations update
* Fri May  5 2017 mardnh@gmx.de
- Update soapy-module version 0.5-2 -> 0.6
* Wed May  3 2017 mardnh@gmx.de
- Update to version 17.02.2
  - Fixed register typo in LMS7002M::SetTxDCOffset()
  - Added option defaults for stream args in SoapyLMS7
  - Fixed LMS7002M::GetPathRFE() currently selected path
* Wed Apr 19 2017 mardnh@gmx.de
- initial package, version 17.02.1
